\section{StreamDB Implementation} \label{StreamDB_Implementation}

StreamDB's architecture is divided into three parts: (1) The Source that responsible for receiving client transaction events and (2) The set of operators that responsible for processing the transaction event and (3) The Sink collects the event result that response to the client, as shown in Figure \ref{fig:streamdb_overview}.We implement the StreamDB cluster as a collection of nodes deployed within the same administrative domain. A node is a single JVM that hosts an StreamDB Operator that contains one or more data tables. We assume that the typical StreamDB node contains one single CPU core, which makes it easier to scale the operator out across multiple nodes to increase both CPU and memory capacity. 

An operator can be scaled out by dividing into multiple, disjoint partitions whose boundaries are based on the values of one (or more) of the table's keys. The StreamDB routes each query to a particular partition based one the values of query parameters using either range partitioning or hash partitioning. Most tables in OLTP applications will be partitioned in this manner. In the TPC example in Figure \ref{fig:streamdb_plans}, most of the tables (e.g. Warehouse, District, Customer, Stock, NewOrder, Order, OrderLine, History) are partitioned on their WAREHOSE id, then all transactions that only access data within a single warehouse will execute as One-Shot. Instead of splitting tables into multiple partitions, the StreamDB can replicate tables across all partitions. Table replication is useful for read-only or heavy-read tables that are accessed together with other tables. For example, the read-only ITEM table in TPC-C in Figure \ref{fig:streamdb_plans} can be replicating on all partitions. By replicating this table, transactions do not need to retrieve data from a remote partition in order to access it. Each node contains one Partition Coordinator that enable operator partition at one node to communicate with other partition nodes in the cluster. Collectively, the coordinators are responsible for ensuring that the StreamDB executes distributed transactions in the correct order and process commit protocol messages for handle multi-partition locks. 

All data tables are stored in main memory using a row-oriented format. StreamDB use the Java standard library implementations of HashMap and TreeMap structures as the default data storage. However, the way of storing transaction data is flexible in StreamDB. Developers can choose what ever data store structure that fits to their application's need. For example, the developer may choose to store the data off-heap (e.g. ChronicleMap, MapDB) to avoid GC problem,  use other existed in-memory embedded database (e.g. Oracle Berkeley DB, H2, Derby)  to provide more flexible queries or even design their own data store structure.

Transaction events are passed through a reliable TCP/IP socket channel that connect different operators. Each operator has one or more input streams, processes the transaction event and produces one or more output streams. Events' serializability is guaranteed by the Timestamp Sequencer as discussed in previous Section \ref{Streaming_Processing}.  An operator function takes the incoming event data defines the processing logic of operator on the transaction according to its stored procedure name. After processing, the operators produce new version of event data and route the new event to the down-stream operator according to the pre-defined transaction DAG.

\subsection{Extended Transaction Supports}

When the transaction is processed in a streaming way, maintaining the strict ACID properties is often challenging, new problems may arise (e.g. Deadlocks problems) and some typical issue may also need to be re-discussed (e.g. Distributed Transactions). 

\subsubsection{Deadlocks}

Moving to a streaming architecture to process transactions may introduces a new performance bottleneck when developer try to split data into more than one operators: coordination for different transactions that may causing deadlock between two operators.

For the simplest scenario, as shown in Figure \ref{fig:streamdb_deadlock_example}, \textit{T1} and \textit{T2} are two different types of transactions that are sent from two different sources. \textit{T1} needs to be processed on \textit{OP1} first and then \textit{OP2}, while \textit{T2} needs to be processed on \textit{OP2} first then \textit{OP1}. Since we introduce \textbf{\textit{Timestamp Sequencer}} inside the operators to maintain the ordering of different transaction streams, both \textit{OP1} and \textit{OP2} need to wait for the upstream data sent from each other in order to decide which transaction to be processed, thus causing the deadlock between those two operators. 

\begin{figure}[h]
	\centering
	\includegraphics[resolution=150]{streamdb_deadlock_example}
	\caption{StreamDB : Simple Deadlock Example}
	\label{fig:streamdb_deadlock_example}
\end{figure}

Such new deadlock problem may only raise when data are split into different operators, which may have high impact on the application's scalability (Similar to distributed transaction). However, we argue that most of such deadlock problems may be avoided by reconstruct the data store schema or change the sequence of transaction stream, e.g. TPC-C workload. If the deadlock is unavoidable, the simplest solution is not to split the data, which is similar to the traditional DBMS solutions but against the StreamDB's original intention - \textit{One Size Fit All}. In order to provide the developer with more flexibility on the application design with StreamDb, we proposes a Global Deadlock Coordinator to offer application with extra scalability compare with the solution developed in the transitional DBMS way.
\begin{figure}[h]
	\centering
	\includegraphics[resolution=150]{streamdb_deadlock_coordinator}
	\caption{StreamDB : Global Deadlock Coordinator}
	\label{fig:streamdb_deadlock_coordinator}
\end{figure}
\begin{itemize}
	\item \textbf{Global Deadlock Coordinator}

The Global Deadlock Coordinator is responsible for receiving transaction notifications from the sources and  forwarding notification to corresponding operators to resolve the deadlock problem. A simple scenario describe how coordinator is working during runtime is shown in Figure \ref{fig:streamdb_deadlock_coordinator} .\\

\item \textbf{Transaction Deadlock Mapping}
\begin{figure}[h]
	\centering
	\includegraphics[resolution=150]{streamdb_deadlock_mapping}
	\caption{StreamDB : Example of Transaction Deadlock Mapping List}
	\label{fig:streamdb_deadlock_mapping}
\end{figure}

When developer design a query plan, the StreamDB is provided with a mapping list of all transactions with associate deadlock informations as shown in Figure \ref{fig:streamdb_deadlock_mapping}.\\


\item \textbf{Deadlock Notification Schema}

\begin{figure}[h]
	\centering
	\includegraphics[resolution=150]{streamdb_deadlock_schema}
	\caption{StreamDB : Deadlock Notification Schema}
	\label{fig:streamdb_deadlock_schema}
\end{figure}

When source receives transaction query from the client, the transaction query need to be checked against the transaction deadlock mapping list. A transaction notification will be send to the Global Deadlock Coordinator only if the transaction exists in the mapping list, which data contains the transaction Stored Procedure name and Timestamp ID, as shown in Figure \ref{fig:streamdb_deadlock_schema}.\\

\begin{figure}
	\begin{algorithmic}[1]
		\Procedure{TimestampsSequencer}{}
		\State $ISs\gets Map[OPId,InputStream]$
		\State $NQs\gets Map[OPId,DeadlockNotificationQueue]$
		\State $Comparator\gets Map[Timestamp,OPId]$
		\State $Iterator\gets ISs.iterator()$
		\While{Iterator.hasNext()}
		\State $IS\gets Iterator.Next()$
		\If{$Comparator.count\not=ISs.count$}
		\If{$IS\not=Null$}
		\If{$IS.OPId.NotIn(Comparator)$}
		\State $Comparator.put(IS.Time)$
		\EndIf
		\Else
		\State $NQ\gets NQs.get(IS.OPId)$
		\State $Notification\gets NQ.take()$
		\If{$Notification\not=Null$}
		\If{$NQ.OPId.NotIn(Comparator)$}
		\State $Comparator.put(Notification.Time)$
		\EndIf
		\EndIf
		\EndIf
		\Else
		\State $OPId\gets Comparator.get(LowestTimestamp)$
		\State $InputStream\gets ISs.get(OPId)$
		\If{$InputStream\not=Null$}
		\State $ProcessTransaction(InputStream)$
		\If{$NQs.get(OPId).take().Time<=LowestTimestamp$}
		\State $RemoveNotificationFromQueue()$
		\EndIf
		\EndIf
		\EndIf
		\If{!Iterator.hasNext()}
		\State $Iterator\gets ISs.iterator()$
		\EndIf
		\EndWhile
		\EndProcedure
	\end{algorithmic}
	\caption{Handling Deadlock in Timestamps Sequencer}\label{fig:streamdb_deadlock_handling}
\end{figure}

\item \textbf{Deadlock Notification Forwarding}

In the current implementation, the Global Deadlock Coordinator is running in a single thread and using the Timestamp Sequencer to maintain the global ordering of notifications received from all the sources. The coordinator will then forward the notifications to operators according to the Transaction Deadlock Mapping List. For example, once the coordinator receive notifications from all the sources, \textit{N(T1)} and \textit{N(T2)}, it will compare the Timestamp for all the notifications and then forward the notification with the lowest timestamp to the associated operator, e.g. forward \textit{N(T1)} to \textit{OP2} and \textit{N(T2)} to \textit{OP1}. \\

\item \textbf{Runtime Deadlock Handling}

Deadlock notifications forward by the Global Deadlock Coordinator will be store in a Deadlock Notification Queue inside the operator. In the case of the operator does not receive any data from one of its input stream, the  Timestamps Sequencer will check the notification queue in order to know when that input stream is expected to receive data in the future. Details of how the Timestamps Sequencer handles the deadlock are shown in Figure \ref{fig:streamdb_deadlock_handling}.

\end{itemize}

\subsubsection{Distributed Transactions}

It is clear that distributed in-stream transaction processing has something to do with transaction processing in distributed DBMS. Many standard transaction processing techniques can be employed by StreamDB, so it is extremely useful to understand classical algorithms of distributed transaction processing and see how it all related to in-stream transaction processing.

Distributed transactions have historically been implemented by the DBMS community in the manner pioneered by the architects of System R* in the 1980s. The primary mechanism by which System R*-style distributed transactions impede throughput and extend latency is the requirement of an agreement protocol between all participating nodes at commit time to ensure atomicity and durability. To ensure isolation, all of a transaction’s locks must be held for the full duration of this agreement protocol, which is typically two-phase commit.

The problem with holding locks during the agreement protocol is that two-phase commit requires multiple network round-trips between all participating machines, and therefore the time required to run the protocol can often be considerably greater than the time required to execute all local transaction processing logic. If a few popularly accessed records are frequently involved in distributed transactions, the resulting extra time that locks are held on these records can have
an extremely deleterious effect on overall transactional throughput.

In StreamDB, we utilize the the similar two-phase commit concurrency control on distributed transactions as what DBMS do. Each operator partition contains one Partition Coordinator, which is responsible for ensuring that the StreamDB executes distributed transactions in the correct order and process commit protocol messages for handle multi-partition locks. However, there are some situations which StreamDB leverages to streamline concurrency control to avoid expensive two-phase commit.

\begin{itemize}
	\item \textbf{Map-Only Distributed Transaction} : If there is no aborts on all the down-stream events, transaction events can process and commit locally. There is no concurrency control and no distributed locks hold on partitions.
	\item \textbf{Map-Reduce Distributed Transaction} : A Map-Reduce distributed transactions enables StreamDB to processing events on multiple partitions and coalesce the result into a single/multiple reduce partitions. This is not the same as processing events on multiple partitions independently (Map-Only) which does not need partition coordination. In this situation, concurrency control can be avoid on the non reduce partitions only if both the following conditions are met: (1) No aborts on all the down-stream events and (2) no aborts on all the reduce partitions.    
\end{itemize}

%\subsubsection{Failure Recovery}

%\begin{itemize}
%	\item \textbf{Log Writer} : Write transaction logs.
%	\\
%	\item \textbf{Replication} : Replicate the query plan to provide high-availability.
%	\\
%	\item \textbf{Data Backup} : Create snapshot and checkpoint for recovery and stream replay.
%\end{itemize}

\begin{figure*}[t]
	\centering
	\includegraphics[width=\linewidth]{streamdb_plans}
	\caption{StreamDB : TPC-C Transactions Query Plans}
	\label{fig:streamdb_plans}
\end{figure*}
