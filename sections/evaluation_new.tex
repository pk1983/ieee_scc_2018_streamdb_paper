%\newpage
\section{Evaluation} \label{StreamDB_Evaluation}

This section presents an evaluation of StreamDB's performance characteristics to explore the main advantages of StreamDB for processing transactions. We use a TPC-C-like benchmarks to examine the throughput, scalability, and latency characteristics of the system under a variety of configurations. We also compare with the TPC-C benchmark implementation of VoltDB, a memory based RDBMS. Furthermore, we analyse the in more details the new order transaction to show how \system can exploit more sophisticated partitioning schemes to improve throughput without incurring in the cost of distributed transactions.

\npar{Experimental Setup} We deployed our implementation on a cluster of four 12-core 2.53GHz Xeon servers with 24GB RAM to serve as Operator nodes, which make up a total of 48 cores and 96 GB RAM. These servers were connected by a gigabit LAN with a network latency of under 0.2ms. Each experiment runs the TPC-C workload for 10 minutes (exclude the warm up period).  The results in this paper present the maximum practical system throughput observed on each configuration. 

% As is typical when increasing request load, StreamDB eventually reaches a point of maximal capacity, at which point overall throughput plateaus.

%\paragraph{Table Partitions \& Replications}

%9 Tables

%\begin{itemize}
%	\item Warehouse
%	\item District - 10 districts per warehouse (W x 10)
%	\item Stock - 100k stocks per warehouse (W x 100k)
%	\item Item - 100k items (100k)
%	\item Customer - 3k customers per district (W x 30k)
%	\item History - One or more history per customer ($>$ W x 30k)
%	\item Order - One or more order per customer ($>$ W x 30k)
%	\item New-Order - Order abort or Order completed ($>$ W x 9k)
%	\item Order-Line - 5 - 10 order-line per order ($>$ W x 300k)
%\end{itemize}

\subsection{Results}

In this section, we show the performance of the TPC-C StreamDB plans presented in the previous section. In order to provide the a comparison between StreamDB and DBMS, we compare with VoltDB, a popular open-source in Memory DBMS, which can ran almost two orders of magnitude faster than traditional DBMS.

\subsubsection{Throughput \& Scalability}

\begin{figure}[h]
	\includegraphics[width=\linewidth]{ex_throughput_new}
	\caption{TPC-Benchmark : Transaction Throughput}
	\label{fig:total_throughput}
\end{figure}

We first investigate the base throughput and scalability of different plans in StreamDB, shown in Figure \ref{fig:total_throughput}. We vary the number of warehouses from 1 to 4 to measure the throughput as data scale out. We measured a linear increase in throughput to a maximum of 15K TPS with the baseline All-In-One-Plan, which increases to 21K TPS with the Optimized-Plan (All Operators are partitioned by W\_ID). The improvement is due to StreamDB ability to exploit more of the machine cores by using pipeline paralleism provided by the Optimized-Plan.  We found that in this setup the throughput is limited by the CPU-bound Operator OpOrder. By partitioning the OpOrder by client both W\_ID and D\_ID (instead of just W\_ID) we observed further improvements, up to 30K TPS. And the operator OpWarehouse becomes the bottleneck in the StreamDB Optimized Plan (W\_ID,D\_ID).

The throughput in VoltDB is much lower, capping at around 10K TPS, which is quite similar to the performance of StreamDB All-In-One-Plan as expected. VoltDB is limited by its coarse-grained partitioning by warehouse (W\_ID), which it requires to avoid the cost of distributed transactions: when we tried to scale the other tables (NewOrder, Orders, OrderLine ... ) base on other keys, the throughput of the VoltDB server drops significantly (as we will show in \S~\ref{sec:flexibility}).

\begin{figure}[h]
	\includegraphics[width=\linewidth]{ex_latency_new}
	\caption{TPC-Benchmark : Average Transaction Latency}
	\label{fig:latency}
\end{figure}
\subsubsection{Latency}

We measure transaction latency as the time between a transaction being received by the StreamDB Source Operator / VoltDB Master Server and corresponding response received by the client application. We report the average transaction latency with 4 warehouses at the maximum throughput, which is illustrated in Figure \ref{fig:latency}. In the TPC-C implementation on StreamDB All-In-One-Plan and VoltDB, the delay of response after full transaction is committed result in similar average transaction latency for all type of transactions in both VoltDB and StreamDB All-In-One plan.

The transaction latency of  New\_Order, Delivery and Payment is substantially lower in the the Optimized Plan due to \system ability to respond to client in advance. In contrast, we observe an increased latency for the Stock\_Level and Order\_Status transaction, due to the latency overhead of data communication among operators. However, the latency requirements for those two transactions in TPC-C are less stringent.

\subsubsection{Flexibility}
\label{sec:flexibility}

\system provides flexible partitioning strategies for different operators which allows to benefit from fine-grained parallelism. In this section we compare the behaviour of \system with VoltDB when executing the new order transaction on a single warehouse on multiple nodes.

\begin{figure}[tbh]
	\centering	\includegraphics[width=\linewidth,keepaspectratio]{ex_plans_partition}
	\caption{StreamDB Transaction Plans (New Order Only) : Horizontal \& Vertical Partitioning}
	\label{fig:p_plans}
\end{figure}

\system allows to execute new order using with an advanced plan focuses more on vertical partitioning: as we observed from the previous experiment results, operator OpWarehouse becomes the bottleneck when operator OpOrder is able to scale out partitioning by district. As shown in Figure~\ref{fig:p_plans}, with vertical partitioning it is possible to scale the operator OpWarehouse out by keeping all the read-only data (which can be replicated), while moving the Non-Read\_only  data (D\_NEXT\_O\_ID in District table and the New\_Order table) into the operator OpOrder, which data can also be partitioned by (W\_ID,D\_ID).

\begin{figure}[tbh]
	\centering	\includegraphics[width=\linewidth,keepaspectratio]{ex_nodes_partition}
	\caption{ Benchmark Specification (1 Partition per Node) : StreamDB VS. VoltDB}
	\label{fig:p_nodes_plans}
\end{figure}

\begin{figure}[tbh]
	\includegraphics[width=\linewidth]{ex_throughput_partition}
	\caption{Partitioning Flexibility Benchmark : Throughput}
	\label{fig:p_throughput}
\end{figure}

\begin{figure}[tbh]
	\includegraphics[width=\linewidth]{ex_latency_partition}
	\caption{Partitioning Flexibility Benchmark : Latency}
	\label{fig:p_latency}
\end{figure}

We compare its performance with VoltDB (Partitioning by W\_ID or (W\_ID,d\_ID)) as we add more nodes to the system. A summary of deployment of each operator partition is presented in  Figure~\ref{fig:p_nodes_plans}, while Figure~\ref{fig:p_throughput} and Figure~\ref{fig:p_latency} reports respectively the throughput and latency results. As expected VoltDB with partitioning by warehouse does not shows improvements as more nodes are added to the system, while partitioning by district severely reduces throughput and increases latency due to the cost of distributed transactions. \system in contrast shows the flexibility of fine-grained partitioning: The baseline Optimised plan (horizontal partitioning) shows a maximum throughput of 13K TPS reached with 2 nodes, limited by the non-scalable operator OpWarehouse, in contrast the throughput for the advanced vertical partitioning plan scales linearly up to 4 sites, reaching 17K TPS. however, its latency is substantially higher than the baseline, since the new order transaction response can only be sent after processing is done in operator OpOrder instead of operator OpWarehouse.

