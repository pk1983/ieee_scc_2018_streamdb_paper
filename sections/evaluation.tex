%\newpage
\section{Evaluation} \label{StreamDB_Evaluation}

This section presents an evaluation of StreamDB's performance characteristics. We use the widely used TPC-C benchmarks to examine the throughput, latency and scaling characteristics of the system under a variety of configurations. We also run the same TPC-C benchmark against the VoltDB, a memory based  RDBMS to exploit the main advantages of StreamDB for processing transactions. Further more, we perform a set of micro-benchmarks to analysis how distributed transaction and deadlock coordination affect the performances in StreamDB, which may motivate further optimization.

\subsection{Experimental Setup}

We deployed our implementation on a cluster of six high-performance 24-core 2.53GHz Xeon servers with 24GB RAM to serve as Operator nodes, which make up a total of 144 cores and 144 GB RAM. These servers were connected by a gigabit LAN with a network latency of under 0.2ms. The results in this paper present the maximum pratical system throughput observed on each configuration. As is typical when increasing request load, StreamDB eventually reaches a point of maximal capacity, at which point overall throughput plateaus.

%\paragraph{Table Partitions \& Replications}

%9 Tables

%\begin{itemize}
%	\item Warehouse
%	\item District - 10 districts per warehouse (W x 10)
%	\item Stock - 100k stocks per warehouse (W x 100k)
%	\item Item - 100k items (100k)
%	\item Customer - 3k customers per district (W x 30k)
%	\item History - One or more history per customer ($>$ W x 30k)
%	\item Order - One or more order per customer ($>$ W x 30k)
%	\item New-Order - Order abort or Order completed ($>$ W x 9k)
%	\item Order-Line - 5 - 10 order-line per order ($>$ W x 300k)
%\end{itemize}

\subsection{TPC-C Implementation in StreamDB}

The performance of OLTP applications on many of the distributed DBMSs, such as VoltDB, depends on the existence of an optimal database design (i.e, partitioning and replication schema). Such a design defines how an application's data and workload is partitioned or replicated across nodes in a cluster, and how transactions are routed to nodes. This in turn determines the number of transactions that access data stored on each node and how skewed the load is across the cluster. Optimizing these two factors is critical to scaling systems: previous works collected experimental evidence that a growing fraction of distributed transactions can degrade performance by over a factor 10x \cite{cao2015database} \cite{pavlo2012skew}. Hence, without a proper design, a distributed DBMS will perform no better than a single-node system due to the overhead caused by blocking, inter-node communication, and load balancing issue, which limit the scaling capacity of current DBMS   \cite{jones2010low}. 

With StreamDB, we are able to overcome such limitations to achieve better performance than traditional DBMS when scaling out, as discussed in previous Section \ref{Streaming_Processing}. However, it is still difficult to imagine that an automatic program could figure out what is the best scaling design for an application. To achieve a optimized plan, we need more knowledge about the application and need to consider additional limitations (e.g. Deadlock, Transaction Aborts) compare with the traditional DBMS. Hence, a knowledgeable expert would have to carefully code the transaction DAGs. It is likely, however, developers can easily start from the simple database design (Similar to the design strategy in traditional DBMS) and advanced to transaction DAG design when they get better understanding the application.

An outline of typical database design strategies (from simple to advance) in StreamDB is as follows:

\begin{enumerate}
	\item Design the database schema (put all tables in one operator) similar to traditional DBMS.
	\item Group up tables with similar scaling need, either CPU or Memory.
	\item Optimize the plan by minimizing Map-Reduce distributed transaction, removing deadlocks and considering possibility of response in advance.
\end{enumerate}

Next, we describe each of these steps in more details by implement the full TPC-C workload in StreamDB, as shown in Figure \ref{fig:streamdb_plans}. \\

\paragraph{\textbf{All In One Plan}} In this first design plan, we simply follow the same database schema as specified in the TPC-C benchmark. All the tables are resided in one operator, which can be partitioned by warehouse ID to scale out. In this case, all the transactions are routed to one operator and a transaction can only be processed after the previous transaction is fully committed. Tables are treated no difference even though they should be optimized by their workload, e.g. Heavy Read-Write Stock table, Mid-weight Insert-Only History table ... And some tables may need extra storage and there is need for scaling out in addition to warehouse ID, e.g. Order Table, Order\_Line Table, History Table.
\\
\paragraph{\textbf{Plan With Deadlock}} Consider the optimization need for different Transactions and Tables, we may try to split the data to stored in different operators. In TPC-C, all transaction classes except New\_Order never need to abort and New\_Order may only need to abort if it contains invalid items. In this case, we can split the tables freely without incur any locking management as long as the Item table is reside in the first operator of New\_Order transaction pipeline. 

In this initial transaction DAG design, we simply take the following points into consideration:

\begin{enumerate}
	\item Group all the Heavy-Read/Read-Only tables into one operator.
	\item Split out the tables with extra scalability need into different operators.
\end{enumerate}

\paragraph{\textbf{Optimized Plan}} Simply split tables into different operators may introduce deadlock among different transactions. For example, in the previous plan, New\_Order transaction and Stock\_Level transaction will lead the operator Stock and Operator Order into deadlock (They need information from each other to decide which transaction to process). To remove the deadlock, we may try to (1) Simply move the whole Stock Table back to Operator Warehouse or (2) Re-construct the Stock Table. With the latter strategy, by looking into the New\_Order transaction, we know that the New\_Order transaction only need to insert in Order\_Line information about the district of a stock entry, which field is never updated. Thus, we can re-construct the Stock Table to move the Read-Only stock entry information into Operator Warehouse. With this new database schema modified to the previous plan, deadlock is removed.

Using the initial design produced in the previous step, we may try to optimize the performance of transaction by considering the unique feature provided by StreamDB - "transaction response in advance". The optimization method in this step is quite straightforward and result in good performance boost in sense of both latency and throughput. In TPC-C, the New\_Order, Payment and Delivery transactions can gain benefit from this approach, as all the information they need to response back to the client only reside in the Operator Warehouse. 

\subsection{Experimental Result}

In the section, we show the results of running the TPC-C benchmark on top of the StreamDB plans presented in the previous section. In order to provide the best comparison between StreamDB and DBMS, we chose to run the TPC-C benchmark against the open-source in Memory DBMS, VoltDB, which claims to be ran almost two orders of magnitude faster than traditional DBMS.

\subsubsection{Throughput \& Scalability}

\begin{figure}[h]
	\includegraphics[width=\linewidth]{total_throughput}
	\caption{TPC-Benchmark : Total Transaction Throughput}
	\label{fig:total_throughput}
\end{figure}

We first investigate the base throughput and scalability of different plans in StreamDB, which results are shown in Figure \ref{fig:total_throughput} and Figure \ref{fig:per_warehouse_throughput}. We varies the number of Warehouse partitions from 1 to 10 to measure the throughput as data scaling out. We measured a maximum throughput of 105,000 TPS (Transaction per Second) with All-In-One-Plan (Map-Only), increasing to 110,000 TPS with Optimized-Plan. In those cases, by avoiding locking overhead, StreamDB delivers linear throughput scaling as the number of partition increase. Throughput was CPU-bound and limited by the Single Source Operator for receiving the client request (Apple to apple comparison with Master-Slaver deployment in DBMS), and we observed twice this throughput as we add additional Source Operator to the cluster. 

There is a huge drop in throughput when scaling data out with the Plan-With-Deadlock, due to the coordination overhead with the single Global Deadlock Coordinator, which stabilized at higher number of partitions. Throughput is low for distributed transactions with All-In-One-Plan (10\% Map-Reduce), owing to the additional locking and communication delay among multiple partitions.

\begin{figure}[h]
	\includegraphics[width=\linewidth]{per_warehouse_throughput}
	\caption{TPC-Benchmark : Per-Warehouse Transaction Throughput}
	\label{fig:per_warehouse_throughput}
\end{figure}

The throughput in VoltDB is much lower with capping at around 20,000 TPS, since TPC-C implementation in VoltDB utilize B-Tree indexing for data storage, which has a O(log (n)) average and worst case search speed. However, in TPC-C benchmarks, none of its transactions require range search. We can then use the Hash indexing, which has a O(1) average search and an O(n) worst case search speed, to optimized the transactions implementation in StreamDB. In this paper, we only show the best case for TPC-C workload that can be optimized with StreamDB. The performance comparison among different indexing techniques for data storage is beyond the scope of this paper.

\begin{figure}[h]
	\includegraphics[width=\linewidth]{latency}
	\caption{TPC-Benchmark : Average Transaction Latency}
	\label{fig:latency}
\end{figure}
\subsubsection{Latency}

In this paper, transaction latency is calculated by how much time elapses between a transaction being received by the StreamDB Source Operator/VoltDB Master Server and corresponding response received by the client application. We measured the Average Transaction Latency with 10 partitions of Warehouse on the maximum throughput, which results are illustrated in Figure \ref{fig:latency}. In the TPC-C implementation on All-In-One-Plan(Map-Only), Plan-With-Deadlock and VoltDB, the delay of response after full transaction is committed result in similar average transaction latency of approximately 150ms for all type of transactions. Distributed transactions latency in All-In-One-Plan(10\% Map-Reduce) was found to be consistently twice the latency of Single-Site/Distributed Map-Only transactions.

With the benefit of "Response in advance" in StreamDB, the latency of New\_Order, Delivery and Payment transaction are reduced significantly, with only an average of 20ms latency. At the same time, no locking is required for all the TPC-C transactions in Optimized Plan in StreamDB, which result in no blocking for processing the transaction stream on all the operators. We observe a 15\%-25\% reduction in latency for the Stock-Level and Order\_Status transaction, due to no waiting is need for the fully commitment of other transactions. 

\subsubsection{Micro Benchmark : Distributed Transactions}

\begin{figure}[h]
	\includegraphics[width=\linewidth]{mapreduce}
	\caption{Micro Benchmark : Distributed Map Reduce Transactions}
	\label{fig:mapreduce}
\end{figure}

The previous experiments examined throughput for 0\%(Map-Only) and 10\%(Map-Reduce) rates of distributed transactions. It is also interesting, however, to examine performance with much higher rates. Figure \ref{fig:mapreduce} shows the impact on throughput of a proportion of distributed transactions, varied between 0\% and 100\%. We use the "All In One Plan" on ten warehouse partitions for running this benchmark. As expected, throughput for StreamDB with Map-Only distributed transactions is independent of lock management overhead, since no locking is used. Throughput for Map-Reduce distributed transactions drop as lock overhead increases and stabilise at some extent of 10,000 transactions per second. However, StreamDB gains higher throughput than VoltDB at the same rate due to the fact that we only hold locks on partial duration of the transaction.

\subsubsection{Micro Benchmark : Global Deadlock Coordinator}

\begin{figure}[h]
	\includegraphics[width=\linewidth]{deadlock}
	\caption{Micro Benchmark : Global Deadlock Coordinator}
	\label{fig:deadlock}
\end{figure}

Figure \ref{fig:deadlock} shows the amount of notifications send from the Global Deadlock Coordinator per second for the "Plan With Deadlock". We observe a maximum total throughput of 200,000 notifications per second which is believed to be the bottleneck of current StreamDB implementation: Single thread Global Deadlock Coordinator without optimization for increasing partitions.

Since the deadlock problem only raise when processing transactions in the streaming way and we argue that in most case it can be resolved by change the sequence of DAG or reconstruct the data structure, the optimization of performance of the Global Deadlock Coordinator is beyond the  scope of this paper. However, improvement should definitely be worth further investigate in the future.

%\subsection{Some Comments Toward Next Generation "One Size Fits All"}

%\begin{itemize}
%	\item The DBMS Approach Is Not The Only Choice
%	\item Data-Centric Is Not The Only Application Development Strategy - Query-Centric? Architecture-Centric?
%\end{itemize}