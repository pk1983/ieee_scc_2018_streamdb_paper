\section{\system Implementation} \label{StreamDB_Implementation}

\system's architecture, shown in Figure \ref{fig:streamdb_overview}, is divided into three parts: (1)~source operators that are responsible for receiving client transaction events, (2)~data operators, responsible for processing transaction events and (3)~sink operators which collect the result events and respond to the client. A \system cluster runs on a collection of nodes, where each node may host one or more sites. A site is a single JVM that hosts a \system Operator that contains one or more partition or replica of a data tables. We assume that a typical \system operator processes event using a single thread, which makes it easier to scale out across multiple nodes to increase both CPU and memory capacity. 

An operator can be scaled out by dividing into multiple, disjoint partitions whose boundaries are based on the values of one (or more) of the table's keys. \system routes each query to a particular partition based one the values of query parameters using either range partitioning or hash partitioning. Most tables in OLTP applications will be partitioned in this way. Instead of splitting tables into multiple partitions, \system can replicate tables across all partitions. Table replication is useful for read-only or heavy-read tables that are accessed together with other tables. By replicating the table, transactions do not need to retrieve data from a remote partition to access it.

Transaction events are passed through a reliable TCP/IP socket channel that connect different operators. Each operator has one or more input streams, processes the transaction event and produces one or more output streams. Events' serializability is guaranteed by the Timestamp Sequencer to ensure the ordering of events received from different up-streams and maintains a consistent view of stored data. The Timestamp Sequencer simply compares the timestamp for all the incoming event streams and then select the event with lowest timestamp for the operator to process.  An operator function takes the incoming event data defining the processing logic of the operator on the transaction according to its stored procedure name. After processing, the operator produces a new event data and route the event to the downstream operator according to the pre-defined transaction graph.

\subsection{TPC-C Implementation in \system}
\label{sec:tpc-c-implementation}

To better understand how transactions are designed, processed and optimized in \system, we begin with a simple example in the typical TPC-C benchmark. TPC-C benchmark contains 5 types of transactions (New Order, Payment, Order Status, Delivery and Stock Level as shown in Table \ref{tab:tpc_c_transaction_new}) \cite{poess2000new}.

\begin{table*}[t]
	\caption{TPC-C Transactions}
	\label{tab:tpc_c_transaction_new}
	\begin{tabularx}{\textwidth}{|p{2.5cm}|c|p{3cm}|X|}
		\hline
		\textbf{Transaction Type}                        &  \textbf{\%}  & \textbf{Visited Tables} & \textbf{Description}                    \\\hline
		New Order                        &  45  & Warehouse, Customer, District, Orders, New\_Order, Item, Stock, Order\_Line &  Customer place a new order. 90\% of all orders can be supplied in full by stocks from the customer's "home" warehouse; 10\% need to access stock belonging to a remote warehouse. Mid-weight read-write transaction with stringent response time requirements to satisfy on-line users. Transaction will be abort only when item not found. \\\hline
		Payment                        &  43 &  Warehouse, District, Customer, History    & Updates the customer's balance and warehouse/district sales fields. 85\% of updates go to customer's home warehouse; 15\% to a remote warehouse. Light-weight Read-write transaction with stringent response time requirements to satisfy on-line users.                 \\\hline
		Order Status                        &  4 & Customer, Orders, Order\_Line,  & Queries the status of a customer's last order. Mid-weight Read only transaction with low response time requirement. \\\hline
		Delivery                        &  4 &  New\_Order, Orders, Order\_Line, Customer          & Process a batch of 10 new (not yet delivered) orders. Each order that is processed will be removed from the New\_Order table and the customer's account balance will be updated. Read-write transaction with relaxed response time requirement.          \\\hline
		Stock Level                       &  4 &  District, Order\_Line, Stock        &  Find the number of recently sold items that have a stock level below a specified threshold. Heavy Read-only transaction with relaxed response time and consistency requirement.          \\\hline
	\end{tabularx}
\end{table*}

The performance of transactional workload applications on many of the distributed DBMSs, such as VoltDB, depends on the existence of an optimal database design (i.e, partitioning and replication schema). Such a design defines how an application's data and workload is partitioned or replicated across sites in a cluster, and how transactions are routed to sites. This in turn determines the number of transactions that access data stored on each site and how skewed the load is across the cluster. Optimizing these two factors by considering both OLTP and OLAP workloads is critical to scaling systems: previous works collected experimental evidence that a growing fraction of distributed transactions can degrade performance by over a factor 10x \cite{cao2015database} \cite{pavlo2012skew}. Hence, without a proper design, a distributed DBMS will perform no better than a single-site system due to the overhead caused by blocking, inter-site communication, and load balancing issue, which limit the scaling capacity of current DBMS   \cite{jones2010low}. 

With \system, we are able to overcome such limitations to achieve better performance than traditional DBMS when scaling out, as discussed in previous Section \ref{Streaming_Processing}. However, it is still difficult to imagine that an automatic program could figure out what is the best scaling design for an application. To achieve a optimized plan, a knowledgeable expert would have to carefully code the transaction graphs. It is likely, however, developers can easily start from the simple database design (Similar to the design strategy in traditional DBMS) and advanced to transaction graph design when they get better understanding the application.

An outline of typical database design strategies (from simple to advanced) in \system is as follows:

\begin{enumerate}
	\item Design the database schema (put all tables in one operator) similar to traditional DBMS.
	\item Group up tables with similar scaling need, either CPU or Memory.
	\item Optimize the plan by minimizing distributed transactions, removing deadlocks and considering possibility of response in advance.
\end{enumerate}

Next, we describe each of these steps in more details by implement the full TPC-C workload in \system, as shown in Figure \ref{fig:streamdb_plans}. \\

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth,keepaspectratio]{streamdb_plans}
	\caption{\system : TPC-C Transactions Query Plans}%\todo{Item and StockData should be replicated also in All-In-One}
	\label{fig:streamdb_plans}
\end{figure}

\paragraph{\textbf{All In One Plan}} In this first design plan, we simply follow the same database schema as specified in the TPC-C benchmark. All the tables resides in one operator, which can be partitioned by warehouse ID to scale out. In this case, all the transactions are routed to one site and a transaction can only be processed after the previous transaction is fully committed. Tables are treated similarly even though they should be optimized by their workload, OLTP or / and OLAP. Some tables may need extra storage and there is need for scaling out in addition to warehouse ID, e.g. Order Table, Order\_Line Table, History Table.
\\
\paragraph{\textbf{Optimized Plan}} Considering the need for optimization for different transactions and tables, it is possible to split the data stored in different operators. In TPC-C, all transaction classes except New\_Order never aborts and New\_Order may only need to abort if it contains invalid items. In this case, we can split the tables freely without incuring in any locking management as long as the Item table resides in the first operator of New\_Order transaction pipeline. 

In this initial transaction graph design, we simply take the following points into consideration:

\begin{enumerate}
	\item Group all the Heavy-Read/Read-Only tables into one operator.
	\item Split out the tables with extra scalability need into different operators.
\end{enumerate}

Using the initial design produced in the previous step, we may try to optimize the performance of transaction by considering the unique feature provided by \system - "transaction response in advance". The optimization method in this step is quite straightforward and result in good performance boost for both latency and throughput. In TPC-C, the New\_Order, Payment and Delivery transactions can benefit from this approach, as all the information they need to respond back to the client only reside in the Operator Warehouse. 